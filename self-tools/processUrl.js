const cheerio = require('cheerio');
var fs = require('fs');
var rootPath = "C:/angular-test/ituni-haitlc/src/assets/html"

LoopDirectoriesRecursive(rootPath)

function LoopDirectoriesRecursive(inputPath){
	fs.readdir( inputPath, function( err, files ) {
		
		
        for(var file of files){               
			var subInputPath = inputPath+"/"+file 
            if(getFiletype(file) === "html")
				processPathInHtmlFile(subInputPath)// addTargetToAHrefInHtmlFile(subInputPath)
        }
    });
}


/* Experimental function */
function processPathInHtmlFile(path){
	fs.readFile(path, 'utf8', function(err, data) {
		if(data){
			$ = cheerio.load(data, {decodeEntities: false})
			$("img").each(function() {
                addVariableToUrl("src", "img", this)
            });		
			$("script").each(function() {
                addVariableToUrl("src", "script", this)
            });		
			$("link").each(function() {
                addVariableToUrl("href", "link", this)
            });	
            $("a").each(function() {
                aHrefAddVariableToUrl("href", "a", this)
            });		
	                                    
			fs.writeFile(path, $.html(), ()=> {console.log("Update ahref success " + path)} )	
		}
	});
}


function addVariableToUrl(attr, type, me){
    var old_src=$(me).attr(attr);
    var new_src = (old_src) ? `processRequestWithHeader('${escape(old_src)}', '${type}', this)` : old_src;
    console.log(new_src);
    $(me).attr("onerror", new_src);            
}

function aHrefAddVariableToUrl(attr, type, me){
    var old_src=$(me).attr(attr);
    var new_src = (old_src) ? `processRequestWithHeader('${escape(old_src)}', '${type}', this)` : old_src;
    console.log(new_src);
    $(me).attr("href", "#");
    $(me).attr("onclick", new_src);            
}

function getFiletype(path){
	var idx = path.lastIndexOf(".") +1
	return path.slice(idx, path.length).toLowerCase()
}