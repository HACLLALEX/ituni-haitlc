import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { HttpClientModule } from '@angular/common/http'; 
import { HttpModule } from '@angular/http';
import { HomePage } from './home';

// native
import { Device } from '@ionic-native/device';
import { NativeStorage } from '@ionic-native/native-storage';
import { SecureStorage } from '@ionic-native/secure-storage';
import { Network } from '@ionic-native/network';
import { InAppBrowser  } from '@ionic-native/in-app-browser';
// import { DocumentViewer  } from '@ionic-native/document-viewer';
import { File  } from '@ionic-native/file';
import { FileOpener  } from '@ionic-native/file-opener';

// shared & environments
import { FetchImagePipe } from '../../shared/fetch-image.pipe';
import { CommonService } from '../../shared/services/common.service';
import { HttpClientService } from '../../shared/services/http-client.service';
import { EnvironmentsModule } from '../../environments/environments.module';

// custom config
import { Config} from '../../shared/service';

const components = [HomePage];

@NgModule({
  declarations: [components],
  imports: [
    IonicPageModule.forChild(HomePage),
    IonicStorageModule.forRoot(),
    HttpModule,
    HttpClientModule,
    EnvironmentsModule
  ],
  entryComponents: components,
  exports: [HomePage],
  providers: [
    Config,
    FetchImagePipe,
    CommonService,
    HttpClientService,
    Device,
    NativeStorage,
    SecureStorage,
    Network,
    InAppBrowser,
    File,
    FileOpener
    // DocumentViewer
  ]  
})
export class HomePageModule {}
