import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, ResponseContentType} from '@angular/http';
import { Device } from '@ionic-native/device';
import { DomSanitizer } from '@angular/platform-browser';

import { CommonService } from './common.service'


@Injectable()
export class HttpClientService {

	constructor(
		public http: Http, private device: Device,
		private commonService: CommonService, private sanitizer: DomSanitizer
	) {
	}

	createAuthorizationHeader(extraHeaders = {}, extraOptions = {}) {
		let deviceId,
			authorization = {},
			userName;
		if (this.commonService.isLocalEnviroment()) {
			deviceId = 'development';
			authorization = { 'authorization': this.commonService.appConfig.keystoneAuthHeader }
			userName = 'devUser';
		}
		else {
			deviceId = this.device.uuid;
			userName = this.commonService.userName.toUpperCase();
		}

		let headers = new Headers({
			'AppID': 'ituni',
			'Content-Type': 'application/json',
			'DeviceID': deviceId,
			'CorpID': userName,
			...authorization,
			...extraHeaders,
		});
  		let options = new RequestOptions({ headers: headers, ...extraOptions });

		return options;
	}

	get(url) {
		return this.http.get(url, this.createAuthorizationHeader());
	}

	post(url, data, extraHeaders = {}) {
		return this.http.post(url, data, this.createAuthorizationHeader(extraHeaders));
	}

	put(url, data) {
		return this.http.put(url, data, this.createAuthorizationHeader());
	}

	delete(url) {
		return this.http.delete(url, this.createAuthorizationHeader());
	}

	getImage(url) {
		
		return this.http.get(url, this.createAuthorizationHeader(
			{'Content-Type': 'image/png'},
			{ responseType: ResponseContentType.Blob }
		))
		.map(res => res.blob())
		.map(blob => {
			var urlCreator = window.URL;
            return  this.sanitizer.bypassSecurityTrustUrl(urlCreator.createObjectURL(blob));
        });
	}

	getDocument(url) {
		return this.http.get(url, this.createAuthorizationHeader(
			{'Content-Type': 'application/pdf'},		//very important!!
			{ responseType: ResponseContentType.Blob }	//very important!!
		))
		.map(res => res.blob())
		.map(blob => {
			var urlCreator = window.URL;
            return  this.sanitizer.bypassSecurityTrustUrl(urlCreator.createObjectURL(blob));
		});
	}	


	getDocumentBlob(url) {
		return this.http.get(url, this.createAuthorizationHeader())
			.toPromise()
			.then(res => {
				console.log(res.headers.get('content-type'))
				return res.headers.get('content-type')
			})
			.then(contentType => {
				return this.http.get(url, this.createAuthorizationHeader(
					{'Content-Type': contentType},				//very important!!
					{ responseType: ResponseContentType.Blob }	//very important!!
				))
				.toPromise()
				.then( res => res.blob() )
			})
	}		
}
